package goNetworkManager

import (
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

type NMDeviceWifi struct {
	*NMDevice
}

//Flags describing the capabilities of a wireless device.
type NMDeviceWifiCapabilities uint32

const (
	WIFI_DEVICE_CAP_NONE          NMDeviceWifiCapabilities = 0x0
	WIFI_DEVICE_CAP_CIPHER_WEP40  NMDeviceWifiCapabilities = 0x1
	WIFI_DEVICE_CAP_CIPHER_WEP104 NMDeviceWifiCapabilities = 0x2
	WIFI_DEVICE_CAP_CIPHER_TKIP   NMDeviceWifiCapabilities = 0x4
	WIFI_DEVICE_CAP_CIPHER_CCMP   NMDeviceWifiCapabilities = 0x8
	WIFI_DEVICE_CAP_WPA           NMDeviceWifiCapabilities = 0x10
	WIFI_DEVICE_CAP_RSN           NMDeviceWifiCapabilities = 0x20
	WIFI_DEVICE_CAP_AP            NMDeviceWifiCapabilities = 0x40
	WIFI_DEVICE_CAP_ADHOC         NMDeviceWifiCapabilities = 0x80
)

func finalizeNMDeviceWifi(nmdw *NMDeviceWifi) {
	nmdw.Close()
}

func NewNMDeviceWifi(ctx context.Context, path dbus.ObjectPath) (nmdw *NMDeviceWifi, err error) {
	obj, err := NewNMObject(ctx, "org.freedesktop.NetworkManager", path, "org.freedesktop.NetworkManager.Device.Wireless")
	if err == nil {
		nmd := new(NMDevice)
		nmd.NMObject = obj
		nmdw = new(NMDeviceWifi)
		nmdw.NMDevice = nmd
		runtime.SetFinalizer(nmdw, finalizeNMDeviceWifi)
	}
	return nmdw, err
}

//Emitted when a new access point is found by the device.
func (nmdw *NMDeviceWifi) AccessPointAdded() <-chan *NMAccessPoint {
	return nmdw.AccessPointAddedCtx(context.Background())
}

func (nmdw *NMDeviceWifi) AccessPointAddedCtx(ctx context.Context) <-chan *NMAccessPoint {
	c := make(chan *NMAccessPoint, 1)
	go nmdw.signalTaskCtx(ctx, "AccessPointAdded", nmdw.accessPointAddedHandlerCtx, c)
	return c
}

//Emitted when an access point disappears from view of the device.
func (nmdw *NMDeviceWifi) AccessPointRemoved() <-chan *NMAccessPoint {
	return nmdw.AccessPointRemovedCtx(context.Background())
}

func (nmdw *NMDeviceWifi) AccessPointRemovedCtx(ctx context.Context) <-chan *NMAccessPoint {
	c := make(chan *NMAccessPoint, 1)
	go nmdw.signalTaskCtx(ctx, "AccessPointRemoved", nmdw.accessPointAddedHandlerCtx, c)
	return c
}

func (nmdw *NMDeviceWifi) accessPointAddedHandlerCtx(ctx context.Context, v []interface{}, c interface{}) {
	if len(v) == 1 {
		if accessPointAddedChan, ok := c.(chan *NMAccessPoint); ok {
			if path, ok := v[0].(dbus.ObjectPath); ok {
				if ap, err := NewNMAccessPoint(nmdw.Ctx(), path); err == nil {
					select {
					case <-ctx.Done():
					case accessPointAddedChan <- ap:
					default:
						<-accessPointAddedChan
						accessPointAddedChan <- ap
					}
				}
			}
		}
	}
}

func (nmdw *NMDeviceWifi) accessPointAddedHandler(v []interface{}, c interface{}) {
	nmdw.accessPointAddedHandlerCtx(context.Background(), v, c)
}

func (nmdw *NMDeviceWifi) accessPointRemovedHandler(v []interface{}, c interface{}) {
	if len(v) == 1 {
		if accessPointRemovedChan, ok := c.(chan *NMAccessPoint); ok {
			if path, ok := v[0].(dbus.ObjectPath); ok {
				if ap, err := NewNMAccessPoint(nmdw.Ctx(), path); err == nil {
					select {
					case accessPointRemovedChan <- ap:
					default:
						<-accessPointRemovedChan
						accessPointRemovedChan <- ap
					}
				}
			}
		}
	}
}

//List of access point object paths.
func (nmdw *NMDeviceWifi) GetAllAccessPoints() (list []*NMAccessPoint, err error) {
	var paths []dbus.ObjectPath
	if err = nmdw.Call(nmdw.iface+".GetAllAccessPoints", 0).Store(&paths); err == nil {
		var ap *NMAccessPoint
		for _, path := range paths {
			if ap, err = NewNMAccessPoint(nmdw.Ctx(), path); err == nil {
				list = append(list, ap)
			} else {
				break
			}
		}
	}
	return list, err
}

//List of access point object paths.
func (nmdw *NMDeviceWifi) GetAccessPoints() (list []*NMAccessPoint, err error) {
	var paths []dbus.ObjectPath
	if err = nmdw.Call(nmdw.iface+".GetAccessPoints", 0).Store(&paths); err == nil {
		var ap *NMAccessPoint
		for _, path := range paths {
			if ap, err = NewNMAccessPoint(nmdw.Ctx(), path); err == nil {
				list = append(list, ap)
			} else {
				break
			}
		}
	}
	return list, err
}

//Request the device to scan
func (nmdw *NMDeviceWifi) RequestScan(options map[string]interface{}) (err error) {
	optionsVariant := make(map[string]dbus.Variant)
	for key, value := range options {
		optionsVariant[key] = dbus.MakeVariant(value)
	}
	return nmdw.Call(nmdw.iface+".GetAllAccessPoints", 0, optionsVariant).Err
}

//The active hardware address of the device.
func (nmdw *NMDeviceWifi) HwAddress() (hwaddress string, err error) {
	var v dbus.Variant
	if err = nmdw.Call("org.freedesktop.DBus.Properties.Get", 0, nmdw.iface, "HwAddress").Store(&v); err == nil {
		var ok bool
		if hwaddress, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return hwaddress, err
}

//The permanent hardware address of the device.
func (nmdw *NMDeviceWifi) PermHwAddress() (hwaddress string, err error) {
	var v dbus.Variant
	if err = nmdw.Call("org.freedesktop.DBus.Properties.Get", 0, nmdw.iface, "PermHwAddress").Store(&v); err == nil {
		var ok bool
		if hwaddress, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return hwaddress, err
}

//The operating mode of the wireless device.
func (nmdw *NMDeviceWifi) Mode() (mode uint32, err error) {
	var v dbus.Variant
	if err = nmdw.Call("org.freedesktop.DBus.Properties.Get", 0, nmdw.iface, "Mode").Store(&v); err == nil {
		var ok bool
		if mode, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		}
	}
	return mode, err
}

//The bit rate currently used by the wireless device, in kilobits/second (Kb/s).
func (nmdw *NMDeviceWifi) Bitrate() (bitrate uint32, err error) {
	var v dbus.Variant
	if err = nmdw.Call("org.freedesktop.DBus.Properties.Get", 0, nmdw.iface, "Bitrate").Store(&v); err == nil {
		var ok bool
		if bitrate, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		}
	}
	return bitrate, err
}

//The capabilities of the wireless device.
func (nmdw *NMDeviceWifi) WirelessCapabilities() (wirelesscapabilities NMDeviceWifiCapabilities, err error) {
	var v dbus.Variant
	var c uint32
	if err = nmdw.Call("org.freedesktop.DBus.Properties.Get", 0, nmdw.iface, "WirelessCapabilities").Store(&v); err == nil {
		var ok bool
		if c, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			wirelesscapabilities = NMDeviceWifiCapabilities(c)
		}
	}
	return wirelesscapabilities, err
}

//List of object paths of access point visible to this wireless device.
func (nmdw *NMDeviceWifi) AccessPoints() (list []*NMAccessPoint, err error) {
	var v dbus.Variant
	if err = nmdw.Call("org.freedesktop.DBus.Properties.Get", 0, nmdw.iface, "AccessPoints").Store(&v); err == nil {
		if paths, ok := v.Value().([]dbus.ObjectPath); ok {
			var ap *NMAccessPoint
			for _, path := range paths {
				if ap, err = NewNMAccessPoint(nmdw.Ctx(), path); err == nil {
					list = append(list, ap)
				} else {
					break
				}
			}
		} else {
			err = PropertyTypeError
		}
	}
	return list, err
}

//Object path of the access point currently used by the wireless device.
func (nmdw *NMDeviceWifi) ActiveAccessPoint() (ap *NMAccessPoint, err error) {
	var v dbus.Variant
	if err = nmdw.Call("org.freedesktop.DBus.Properties.Get", 0, nmdw.iface, "ActiveAccessPoint").Store(&v); err == nil {
		if path, ok := v.Value().(dbus.ObjectPath); ok {
			ap, err = NewNMAccessPoint(nmdw.Ctx(), path)
		} else {
			err = PropertyTypeError
		}
	}
	return ap, err
}
