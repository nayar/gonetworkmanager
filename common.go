package goNetworkManager

import (
	"errors"
	"fmt"
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

type NMObject struct {
	dbus.BusObject
	bus       *dbus.Conn
	busClosed bool
	iface     string
	ctx       context.Context
	cancelCtx context.CancelFunc
}

type signalHandler func(v []interface{}, c interface{})
type signalHandlerCtx func(ctx context.Context, v []interface{}, c interface{})

var PropertyTypeError error = errors.New("Conversion error while getting D-Bus property")

func finalizeNMObject(obj *NMObject) {
	obj.Close()
}

func NewNMObject(ctx context.Context, destination string, path dbus.ObjectPath, iface string) (obj *NMObject, err error) {
	var bus *dbus.Conn
	obj = new(NMObject)
	obj.iface = iface
	obj.ctx, obj.cancelCtx = context.WithCancel(ctx)
	runtime.SetFinalizer(obj, finalizeNMObject)
	if bus, err = dbus.SystemBusPrivate(); err != nil {
		obj.cancelCtx()
		return nil, err
	}
	obj.BusObject = bus.Object(destination, path)
	obj.bus = bus
	if err = bus.Auth(nil); err != nil {
		obj.Close()
		return nil, err
	}
	if err = bus.Hello(); err != nil {
		obj.Close()
		return nil, err
	}

	go obj.busCloseWatcher()

	return obj, err
}

func (obj *NMObject) busCloseWatcher() {
	select {
	case <-obj.ctx.Done():
		if !obj.busClosed {
			obj.bus.Close()
			obj.busClosed = true
		}
	}
}

func (obj *NMObject) signalTaskCtx(ctx context.Context, name string, handler signalHandlerCtx, signalChan interface{}) {
	defer func() {
		recover()
	}()
	obj.bus.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, fmt.Sprintf("type='signal',interface='%s',member=%s", obj.iface, name))
	defer obj.bus.BusObject().Call("org.freedesktop.DBus.RemoveMatch", 0, fmt.Sprintf("type='signal',interface='%s',member=%s", obj.iface, name))
	c := make(chan *dbus.Signal, 128)
	defer close(c)
	obj.bus.Signal(c)
	defer obj.bus.RemoveSignal(c)

	for {
		select {
		case <-ctx.Done():
			return
		case <-obj.ctx.Done():
			return
		case signal, ok := <-c:
			if !ok {
				return
			} else if signal.Name == fmt.Sprintf("%s.%s", obj.iface, name) && signal.Path == obj.Path() {
				handler(ctx, signal.Body, signalChan)
			}
		}
	}
}

func (obj *NMObject) signalTask(name string, handler signalHandler, signalChan interface{}) {
	h := func(ctx context.Context, v []interface{}, c interface{}) { handler(v, c) }
	obj.signalTaskCtx(context.Background(), name, h, signalChan)
}

func (obj *NMObject) PropertiesChanged() <-chan map[string]interface{} {
	return obj.PropertiesChangedCtx(context.Background())
}

func (obj *NMObject) PropertiesChangedCtx(ctx context.Context) <-chan map[string]interface{} {
	c := make(chan map[string]interface{}, 16)
	go obj.signalTaskCtx(ctx, "PropertiesChanged", obj.propertiesChangedHandlerCtx, c)
	return c
}

func (obj *NMObject) propertiesChangedHandlerCtx(ctx context.Context, v []interface{}, c interface{}) {
	if len(v) == 1 {
		if propertiesChangedChan, ok := c.(chan map[string]interface{}); ok {
			if propertiesVariant, ok := v[0].(map[string]dbus.Variant); ok {
				properties := make(map[string]interface{})
				for key, value := range propertiesVariant {
					properties[key] = value.Value()
				}
				select {
				case <-ctx.Done():
				case propertiesChangedChan <- properties:
				default:
					<-propertiesChangedChan
					propertiesChangedChan <- properties
				}
			}
		}
	}
}

func (obj *NMObject) propertiesChangedHandler(v []interface{}, c interface{}) {
	obj.propertiesChangedHandlerCtx(context.Background(), v, c)
}

func (obj *NMObject) Ctx() context.Context {
	return obj.ctx
}

func (obj *NMObject) Close() (err error) {
	obj.cancelCtx()
	if !obj.busClosed {
		err = obj.bus.Close()
		obj.busClosed = true
	}
	return err
}
