package goNetworkManager

import (
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

type NMSettingsConnection struct {
	*NMObject
}

func finalizeNMSettingsConnection(nmcs *NMSettingsConnection) {
	nmcs.Close()
}

func NewNMSettingsConnection(ctx context.Context, path dbus.ObjectPath) (nmcs *NMSettingsConnection, err error) {
	obj, err := NewNMObject(ctx, "org.freedesktop.NetworkManager", path, "org.freedesktop.NetworkManager.Settings.Connection")
	if err == nil {
		nmcs = new(NMSettingsConnection)
		nmcs.NMObject = obj
		runtime.SetFinalizer(nmcs, finalizeNMSettingsConnection)
	}
	return nmcs, err
}

func (nmcs *NMSettingsConnection) Updated(ctx context.Context) <-chan struct{} {
	c := make(chan struct{}, 1)
	go nmcs.signalTaskCtx(ctx, "Updated", nmcs.updatedHandlerCtx, c)
	return c
}

func (nmcs *NMSettingsConnection) Removed(ctx context.Context) <-chan struct{} {
	c := make(chan struct{}, 1)
	go nmcs.signalTaskCtx(ctx, "Removed", nmcs.removedHandlerCtx, c)
	return c
}

func (nmcs *NMSettingsConnection) updatedHandlerCtx(ctx context.Context, v []interface{}, c interface{}) {
	if updatedChan, ok := c.(chan struct{}); ok {
		select {
		case <-ctx.Done():
		case updatedChan <- struct{}{}:
		default:
		}
	}
}

func (nmcs *NMSettingsConnection) updatedHandler(v []interface{}, c interface{}) {
	nmcs.updatedHandlerCtx(context.Background(), v, c)
}

func (nmcs *NMSettingsConnection) removedHandlerCtx(ctx context.Context, v []interface{}, c interface{}) {
	if removedChan, ok := c.(chan struct{}); ok {
		select {
		case <-ctx.Done():
		case removedChan <- struct{}{}:
		default:
		}
	}
}

func (nmcs *NMSettingsConnection) removedHandler(v []interface{}, c interface{}) {
	nmcs.removedHandlerCtx(context.Background(), v, c)
}

func (nmcs *NMSettingsConnection) Update(properties map[string]map[string]interface{}) (err error) {
	propertiesVariant := make(map[string]map[string]dbus.Variant)
	for k1, _ := range properties {
		propertiesVariant[k1] = make(map[string]dbus.Variant)
		for k2, v := range properties[k1] {
			propertiesVariant[k1][k2] = dbus.MakeVariant(v)
		}
	}
	return nmcs.Call(nmcs.iface+".Update", 0, propertiesVariant).Err
}

func (nmcs *NMSettingsConnection) UpdateUnsaved(properties map[string]map[string]dbus.Variant) (err error) {
	propertiesVariant := make(map[string]map[string]dbus.Variant)
	for k1, _ := range properties {
		propertiesVariant[k1] = make(map[string]dbus.Variant)
		for k2, v := range properties[k1] {
			propertiesVariant[k1][k2] = dbus.MakeVariant(v)
		}
	}
	return nmcs.Call(nmcs.iface+".UpdateUnsaved", 0, propertiesVariant).Err
}

func (nmcs *NMSettingsConnection) Delete() (err error) {
	return nmcs.Call(nmcs.iface+".Delete", 0).Err
}

func (nmcs *NMSettingsConnection) GetSettings() (settings map[string]map[string]interface{}, err error) {
	settingsVariant := make(map[string]map[string]dbus.Variant)
	settings = make(map[string]map[string]interface{})
	err = nmcs.Call(nmcs.iface+".GetSettings", 0).Store(&settingsVariant)
	for k1, _ := range settingsVariant {
		settings[k1] = make(map[string]interface{})
		for k2, v := range settingsVariant[k1] {
			settings[k1][k2] = v.Value()
		}
	}
	return settings, err
}

func (nmcs *NMSettingsConnection) GetSecrets(setting string) (secrets map[string]map[string]interface{}, err error) {
	secretsVariant := make(map[string]map[string]dbus.Variant)
	secrets = make(map[string]map[string]interface{})
	err = nmcs.Call(nmcs.iface+".GetSecrets", 0, setting).Store(&secretsVariant)
	for k1, _ := range secretsVariant {
		secrets[k1] = make(map[string]interface{})
		for k2, v := range secretsVariant[k1] {
			secrets[k1][k2] = v.Value()
		}
	}
	return secrets, err
}

func (nmcs *NMSettingsConnection) Save() (err error) {
	return nmcs.Call(nmcs.iface+".Save", 0).Err
}

func (nmcs *NMSettingsConnection) Unsaved() (unsaved bool, err error) {
	var v dbus.Variant
	if err = nmcs.Call("org.freedesktop.DBus.Properties.Get", 0, nmcs.iface, "Unsaved").Store(&v); err == nil {
		var ok bool
		if unsaved, ok = v.Value().(bool); !ok {
			err = PropertyTypeError
		}
	}
	return unsaved, err
}
