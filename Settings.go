package goNetworkManager

import (
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

type NMSettings struct {
	*NMObject
}

func finalizeNMSettings(nms *NMSettings) {
	nms.Close()
}

func NewNMSettings(ctx context.Context) (nms *NMSettings, err error) {
	obj, err := NewNMObject(ctx, "org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager/Settings", "org.freedesktop.NetworkManager.Settings")
	if err == nil {
		nms = new(NMSettings)
		nms.NMObject = obj
		runtime.SetFinalizer(nms, finalizeNMSettings)
	}
	return nms, err
}

func (nms *NMSettings) NewConnection(ctx context.Context) <-chan *NMSettingsConnection {
	c := make(chan *NMSettingsConnection, 1)
	go nms.signalTaskCtx(ctx, "NewConnection", nms.newConnectionHandler, c)
	return c
}

func (nms *NMSettings) ConnectionRemoved(ctx context.Context) <-chan *NMSettingsConnection {
	c := make(chan *NMSettingsConnection, 1)
	go nms.signalTaskCtx(ctx, "ConnectionRemoved", nms.connectionRemovedHandler, c)
	return c
}

func (nms *NMSettings) newConnectionHandler(ctx context.Context, v []interface{}, c interface{}) {
	if len(v) == 1 {
		if newConnectionChan, ok := c.(chan *NMSettingsConnection); ok {
			if path, ok := v[0].(dbus.ObjectPath); ok {

				if conn, err := NewNMSettingsConnection(ctx, path); err == nil {
					select {
					case newConnectionChan <- conn:
					default:
						<-newConnectionChan
						newConnectionChan <- conn
					}
				}
			}
		}
	}
}

func (nms *NMSettings) connectionRemovedHandler(ctx context.Context, v []interface{}, c interface{}) {
	if len(v) == 1 {
		if connectionRemovedChan, ok := c.(chan *NMSettingsConnection); ok {
			if path, ok := v[0].(dbus.ObjectPath); ok {
				if conn, err := NewNMSettingsConnection(ctx, path); err == nil {
					select {
					case connectionRemovedChan <- conn:
					default:
						<-connectionRemovedChan
						connectionRemovedChan <- conn
					}
				}
			}
		}
	}
}

func (nms *NMSettings) ListConnections(ctx context.Context) (list []*NMSettingsConnection, err error) {
	var paths []dbus.ObjectPath
	if err = nms.Call(nms.iface+".ListConnections", 0).Store(&paths); err == nil {
		var conn *NMSettingsConnection
		for _, path := range paths {
			if conn, err = NewNMSettingsConnection(ctx, path); err == nil {
				list = append(list, conn)
			} else {
				break
			}
		}
	}
	return list, err
}

func (nms *NMSettings) GetConnectionByUuid(ctx context.Context, uuid string) (conn *NMSettingsConnection, err error) {
	var path dbus.ObjectPath
	if err = nms.Call(nms.iface+".GetConnectionByUuid", 0, uuid).Store(&path); err == nil {
		conn, err = NewNMSettingsConnection(ctx, path)
	}
	return conn, err
}

func (nms *NMSettings) AddConnection(ctx context.Context, connection map[string]map[string]interface{}) (conn *NMSettingsConnection, err error) {
	var path dbus.ObjectPath
	connectionVariant := make(map[string]map[string]dbus.Variant)
	for k1, _ := range connection {
		connectionVariant[k1] = make(map[string]dbus.Variant)
		for k2, v := range connection[k1] {
			connectionVariant[k1][k2] = dbus.MakeVariant(v)
		}
	}
	if err = nms.Call(nms.iface+".AddConnection", 0, connectionVariant).Store(&path); err == nil {
		conn, err = NewNMSettingsConnection(ctx, path)
	}
	return conn, err
}

func (nms *NMSettings) AddConnectionUnsaved(ctx context.Context, connection map[string]map[string]interface{}) (conn *NMSettingsConnection, err error) {
	var path dbus.ObjectPath
	connectionVariant := make(map[string]map[string]dbus.Variant)
	for k1, _ := range connection {
		connectionVariant[k1] = make(map[string]dbus.Variant)
		for k2, v := range connection[k1] {
			connectionVariant[k1][k2] = dbus.MakeVariant(v)
		}
	}
	if err = nms.Call(nms.iface+".AddConnectionUnsaved", 0, connectionVariant).Store(&path); err == nil {
		conn, err = NewNMSettingsConnection(ctx, path)
	}
	return conn, err
}

func (nms *NMSettings) LoadConnections(filenames []string) (status bool, failures []string, err error) {
	err = nms.Call(nms.iface+".LoadConnections", 0, filenames).Store(&status, &failures)
	return status, failures, err
}

func (nms *NMSettings) ReloadConnections() (status bool, err error) {
	err = nms.Call(nms.iface+".ReloadConnections", 0).Store(&status)
	return status, err
}

func (nms *NMSettings) SaveHostname(hostname string) (err error) {
	return nms.Call(nms.iface+".SaveHostname", 0, hostname).Err
}

func (nms *NMSettings) Connections(ctx context.Context) (list []*NMSettingsConnection, err error) {
	var v dbus.Variant
	if err = nms.Call("org.freedesktop.DBus.Properties.Get", 0, nms.iface, "Connections").Store(&v); err == nil {
		if paths, ok := v.Value().([]dbus.ObjectPath); ok {
			var conn *NMSettingsConnection
			for _, path := range paths {
				if conn, err = NewNMSettingsConnection(ctx, path); err == nil {
					list = append(list, conn)
				} else {
					break
				}
			}
		} else {
			err = PropertyTypeError
		}
	}
	return list, err
}

func (nms *NMSettings) Hostname() (hostname string, err error) {
	var v dbus.Variant
	if err = nms.Call("org.freedesktop.DBus.Properties.Get", 0, nms.iface, "Hostname").Store(&v); err == nil {
		var ok bool
		if hostname, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return hostname, err
}

func (nms *NMSettings) CanModify() (canmodify bool, err error) {
	var v dbus.Variant
	if err = nms.Call("org.freedesktop.DBus.Properties.Get", 0, nms.iface, "CanModify").Store(&v); err == nil {
		var ok bool
		if canmodify, ok = v.Value().(bool); !ok {
			err = PropertyTypeError
		}
	}
	return canmodify, err
}
