package goNetworkManager

import (
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

type NMDeviceEthernet struct {
	*NMDevice
}

func finalizeNMDeviceEthernet(nmde *NMDeviceEthernet) {
	nmde.Close()
}

func NewNMDeviceEthernet(ctx context.Context, path dbus.ObjectPath) (nmde *NMDeviceEthernet, err error) {
	obj, err := NewNMObject(ctx, "org.freedesktop.NetworkManager", path, "org.freedesktop.NetworkManager.Device.Wired")
	if err == nil {
		nmd := new(NMDevice)
		nmd.NMObject = obj
		nmde = new(NMDeviceEthernet)
		nmde.NMDevice = nmd
		runtime.SetFinalizer(nmde, finalizeNMDeviceEthernet)
	}
	return nmde, err
}

//Active hardware address of the device.
func (nmde *NMDeviceEthernet) HwAddress() (hwaddress string, err error) {
	var v dbus.Variant
	if err = nmde.Call("org.freedesktop.DBus.Properties.Get", 0, nmde.iface, "HwAddress").Store(&v); err == nil {
		var ok bool
		if hwaddress, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return hwaddress, err
}

//Permanent hardware address of the device.
func (nmde *NMDeviceEthernet) PermHwAddress() (hwaddress string, err error) {
	var v dbus.Variant
	if err = nmde.Call("org.freedesktop.DBus.Properties.Get", 0, nmde.iface, "PermHwAddress").Store(&v); err == nil {
		var ok bool
		if hwaddress, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return hwaddress, err
}

//Design speed of the device, in megabits/second (Mb/s).
func (nmde *NMDeviceEthernet) Speed() (speed uint32, err error) {
	var v dbus.Variant
	if err = nmde.Call("org.freedesktop.DBus.Properties.Get", 0, nmde.iface, "Speed").Store(&v); err == nil {
		var ok bool
		if speed, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		}
	}
	return speed, err
}

//Indicates whether the physical carrier is found (e.g. whether a cable is plugged in or not).
func (nmde *NMDeviceEthernet) Carrier() (carrier bool, err error) {
	var v dbus.Variant
	if err = nmde.Call("org.freedesktop.DBus.Properties.Get", 0, nmde.iface, "Carrier").Store(&v); err == nil {
		var ok bool
		if carrier, ok = v.Value().(bool); !ok {
			err = PropertyTypeError
		}
	}
	return carrier, err
}
