package goNetworkManager

import (
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

type StateChange struct {
	NewState NMDeviceState
	OldState NMDeviceState
	Reason   uint32
}

type NMDevice struct {
	*NMObject
}

type NMDeviceState uint32

const (
	DEVICE_STATE_UNKNOWN      NMDeviceState = 0
	DEVICE_STATE_UNMANAGED    NMDeviceState = 10
	DEVICE_STATE_UNAVAILABLE  NMDeviceState = 20
	DEVICE_STATE_DISCONNECTED NMDeviceState = 30
	DEVICE_STATE_PREPARE      NMDeviceState = 40
	DEVICE_STATE_CONFIG       NMDeviceState = 50
	DEVICE_STATE_NEED_AUTH    NMDeviceState = 60
	DEVICE_STATE_IP_CONFIG    NMDeviceState = 70
	DEVICE_STATE_IP_CHECK     NMDeviceState = 80
	DEVICE_STATE_SECONDARIES  NMDeviceState = 90
	DEVICE_STATE_ACTIVATED    NMDeviceState = 100
	DEVICE_STATE_DEACTIVATING NMDeviceState = 110
	DEVICE_STATE_FAILED       NMDeviceState = 120
)

type NMDeviceType uint32

const (
	DEVICE_TYPE_UNKNOWN    NMDeviceType = 0
	DEVICE_TYPE_ETHERNET   NMDeviceType = 1
	DEVICE_TYPE_WIFI       NMDeviceType = 2
	DEVICE_TYPE_UNUSED1    NMDeviceType = 3
	DEVICE_TYPE_UNUSED2    NMDeviceType = 4
	DEVICE_TYPE_BT         NMDeviceType = 5
	DEVICE_TYPE_OLPC_MESH  NMDeviceType = 6
	DEVICE_TYPE_WIMAX      NMDeviceType = 7
	DEVICE_TYPE_MODEM      NMDeviceType = 8
	DEVICE_TYPE_INFINIBAND NMDeviceType = 9
	DEVICE_TYPE_BOND       NMDeviceType = 10
	DEVICE_TYPE_VLAN       NMDeviceType = 11
	DEVICE_TYPE_ADSL       NMDeviceType = 12
	DEVICE_TYPE_BRIDGE     NMDeviceType = 13
	DEVICE_TYPE_GENERIC    NMDeviceType = 14
	DEVICE_TYPE_TEAM       NMDeviceType = 15
)

type NMDeviceCapabilities uint32

const (
	DEVICE_CAP_NONE           NMDeviceCapabilities = 0x0
	DEVICE_CAP_NM_SUPPORTED   NMDeviceCapabilities = 0x1
	DEVICE_CAP_CARRIER_DETECT NMDeviceCapabilities = 0x2
)

func finalizeNMDevice(nmd *NMDevice) {
	nmd.Close()
}

func NewNMDevice(ctx context.Context, path dbus.ObjectPath) (nmd *NMDevice, err error) {
	obj, err := NewNMObject(ctx, "org.freedesktop.NetworkManager", path, "org.freedesktop.NetworkManager.Device")
	if err == nil {
		nmd = new(NMDevice)
		nmd.NMObject = obj
		runtime.SetFinalizer(nmd, finalizeNMDevice)
	}
	return nmd, err
}

func (nmd *NMDevice) StateChanged(ctx context.Context) <-chan StateChange {
	c := make(chan StateChange, 1)
	go nmd.signalTaskCtx(ctx, "StateChanged", nmd.stateChangedHandlerCtx, c)
	return c
}

func (nmd *NMDevice) stateChangedHandlerCtx(ctx context.Context, v []interface{}, c interface{}) {
	if len(v) == 3 {
		if stateChangedChan, ok := c.(chan StateChange); ok {
			newState, _ := v[0].(uint32)
			oldState, _ := v[1].(uint32)
			reason, _ := v[2].(uint32)
			stateChange := StateChange{NewState: NMDeviceState(newState), OldState: NMDeviceState(oldState), Reason: reason}
			select {
			case <-ctx.Done():
			case stateChangedChan <- stateChange:
			default:
				<-stateChangedChan
				stateChangedChan <- stateChange
			}
		}
	}
}

func (nmd *NMDevice) stateChangedHandler(v []interface{}, c interface{}) {
	nmd.stateChangedHandlerCtx(context.Background(), v, c)
}

//Disconnects a device and prevents the device from automatically activating further connections without user intervention.
func (nmd *NMDevice) Disconnect() (err error) {
	return nmd.Call(nmd.iface+".Disconnect", 0).Err
}

//Operating-system specific transient device hardware identifier.  This
//is an opaque string representing the underlying hardware for the device,
//and shouldn't be used to keep track of individual devices.  For some
//device types (Bluetooth, Modems) it is an identifier used by the
//hardware service (ie bluez or ModemManager) to refer to that device,
//and client programs use it get additional information from those
//services which NM does not provide.  The Udi is not guaranteed to be
//consistent across reboots or hotplugs of the hardware. If you're looking
//for a way to uniquely track each device in your application, use the
//object path.  If you're looking for a way to track a specific piece of
//hardware across reboot or hotplug, use a MAC address or USB serial
//number.
func (nmd *NMDevice) Udi() (udi string, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "Udi").Store(&v); err == nil {
		var ok bool
		if udi, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return udi, err
}

//The name of the device's control (and often data) interface.
func (nmd *NMDevice) Interface() (iface string, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "Interface").Store(&v); err == nil {
		var ok bool
		if iface, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return iface, err
}

//The name of the device's data interface when available.  This property
//may not refer to the actual data interface until the device has
//successfully established a data connection, indicated by the device's
//State becoming ACTIVATED.
func (nmd *NMDevice) IpInterface() (iface string, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "IpInterface").Store(&v); err == nil {
		var ok bool
		if iface, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return iface, err
}

//The driver handling the device.
func (nmd *NMDevice) Driver() (driver string, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "Driver").Store(&v); err == nil {
		var ok bool
		if driver, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return driver, err
}

//The version of the driver handling the device.
func (nmd *NMDevice) DriverVersion() (version string, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "DriverVersion").Store(&v); err == nil {
		var ok bool
		if version, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return version, err
}

//The firmware version for the device.
func (nmd *NMDevice) FirmwareVersion() (firm string, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "FirmwareVersion").Store(&v); err == nil {
		var ok bool
		if firm, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return firm, err
}

//Flags describing the capabilities of the device.
func (nmd *NMDevice) Capabilities() (capabilities NMDeviceCapabilities, err error) {
	var v dbus.Variant
	var c uint32
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "Capabilities").Store(&v); err == nil {
		var ok bool
		if c, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			capabilities = NMDeviceCapabilities(c)
		}
	}
	return capabilities, err
}

//The current state of the device.
func (nmd *NMDevice) State() (state NMDeviceState, err error) {
	var v dbus.Variant
	var s uint32
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "State").Store(&v); err == nil {
		var ok bool
		if s, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			state = NMDeviceState(s)
		}
	}
	return state, err
}

//Object path of an ActiveConnection object that "owns" this device during
//activation.  The ActiveConnection object tracks the life-cycle of a
//connection to a specific network and implements the
//org.freedesktop.NetworkManager.Connection.Active D-Bus interface.
func (nmd *NMDevice) ActiveConnection() (active_connection *NMActiveConnection, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "ActiveConnection").Store(&v); err == nil {
		if path, ok := v.Value().(dbus.ObjectPath); ok {
			active_connection, err = NewNMActiveConnection(nmd.Ctx(), path)
		} else {
			err = PropertyTypeError
		}
	}
	return active_connection, err
}

//Whether or not this device is managed by NetworkManager.
func (nmd *NMDevice) Managed() (managed bool, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "Managed").Store(&v); err == nil {
		var ok bool
		if managed, ok = v.Value().(bool); !ok {
			err = PropertyTypeError
		}
	}
	return managed, err
}

//If TRUE, indicates the device is allowed to autoconnect.  If FALSE,
//manual intervention is required before the device will automatically
//connect to a known network, such as activating a connection using the
//device, or setting this property to TRUE.
func (nmd *NMDevice) Autoconnect() (autoconnect bool, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "Autoconnect").Store(&v); err == nil {
		var ok bool
		if autoconnect, ok = v.Value().(bool); !ok {
			err = PropertyTypeError
		}
	}
	return autoconnect, err
}

func (nmd *NMDevice) SetAutoconnect(autoconnect bool) (err error) {
	v := dbus.MakeVariant(autoconnect)
	return nmd.Call("org.freedesktop.DBus.Properties.Set", 0, nmd.iface, "Autoconnect", v).Err
}

//If TRUE, indicates the device is likely missing firmware necessary for
//its operation.
func (nmd *NMDevice) FirmwareMissing() (missing bool, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "FirmwareMissing").Store(&v); err == nil {
		var ok bool
		if missing, ok = v.Value().(bool); !ok {
			err = PropertyTypeError
		}
	}
	return missing, err
}

//The general type of the network device; ie Ethernet, WiFi, etc.
func (nmd *NMDevice) DeviceType() (devicetype NMDeviceType, err error) {
	var v dbus.Variant
	var t uint32
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "DeviceType").Store(&v); err == nil {
		var ok bool
		if t, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			devicetype = NMDeviceType(t)
		}
	}
	return devicetype, err
}

//An array of object paths of every configured connection that is currently 'available' through this device.
func (nmd *NMDevice) AvailableConnections() (list []*NMSettingsConnection, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "AvailableConnections").Store(&v); err == nil {
		if paths, ok := v.Value().([]dbus.ObjectPath); ok {
			for _, path := range paths {
				var conn *NMSettingsConnection
				if conn, err = NewNMSettingsConnection(nmd.Ctx(), path); err == nil {
					list = append(list, conn)
				} else {
					break
				}
			}
		} else {
			err = PropertyTypeError
		}
	}
	return list, err
}

//If non-empty, an (opaque) indicator of the physical network
//port associated with the device. This can be used to recognize
//when two seemingly-separate hardware devices are actually just
//different virtual interfaces to the same physical port.
func (nmd *NMDevice) PhysicalPortId() (id string, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "PhysicalPortId").Store(&v); err == nil {
		var ok bool
		if id, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return id, err
}

//The device MTU (maximum transmission unit).
func (nmd *NMDevice) Mtu() (mtu uint32, err error) {
	var v dbus.Variant
	if err = nmd.Call("org.freedesktop.DBus.Properties.Get", 0, nmd.iface, "Mtu").Store(&v); err == nil {
		var ok bool
		if mtu, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		}
	}
	return mtu, err
}

func (nmd *NMDevice) Ethernet() (*NMDeviceEthernet, error) {
	return NewNMDeviceEthernet(nmd.Ctx(), nmd.Path())
}

func (nmd *NMDevice) Modem() (*NMDeviceModem, error) {
	return NewNMDeviceModem(nmd.Ctx(), nmd.Path())
}

func (nmd *NMDevice) WiFi() (*NMDeviceWifi, error) {
	return NewNMDeviceWifi(nmd.Ctx(), nmd.Path())
}
