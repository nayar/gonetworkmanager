package goNetworkManager

import (
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

//Objects that implement the Connection.Active interface represent an attempt
//to connect to a network using the details provided by a Connection object.
//The Connection.Active object tracks the life-cycle of the connection
//attempt and if successful indicates whether the connected network is the
//"default" or preferred network for access.

type NMActiveConnection struct {
	*NMObject
}

type NMActiveConnectionState uint32

const (
	ACTIVE_CONNECTION_STATE_UNKNOWN      NMActiveConnectionState = 0
	ACTIVE_CONNECTION_STATE_ACTIVATING   NMActiveConnectionState = 1
	ACTIVE_CONNECTION_STATE_ACTIVATED    NMActiveConnectionState = 2
	ACTIVE_CONNECTION_STATE_DEACTIVATING NMActiveConnectionState = 3
	ACTIVE_CONNECTION_STATE_DEACTIVATED  NMActiveConnectionState = 4
)

func finalizeNMActiveConnection(nmac *NMActiveConnection) {
	nmac.Close()
}

func NewNMActiveConnection(ctx context.Context, path dbus.ObjectPath) (nmac *NMActiveConnection, err error) {
	obj, err := NewNMObject(ctx, "org.freedesktop.NetworkManager", path, "org.freedesktop.NetworkManager.Connection.Active")
	if err == nil {
		nmac = new(NMActiveConnection)
		nmac.NMObject = obj
		runtime.SetFinalizer(nmac, finalizeNMActiveConnection)
	}
	return nmac, err
}

//The path of the connection.
func (nmac *NMActiveConnection) Connection() (conn *NMSettingsConnection, err error) {
	var v dbus.Variant
	if err = nmac.Call("org.freedesktop.DBus.Properties.Get", 0, nmac.iface, "Connection").Store(&v); err == nil {
		if path, ok := v.Value().(dbus.ObjectPath); ok {
			conn, err = NewNMSettingsConnection(nmac.Ctx(), path)
		} else {
			err = PropertyTypeError
		}
	}
	return conn, err
}

//A specific object associated with the active connection.  This property
//reflects the specific object used during connection activation, and will
//not change over the lifetime of the ActiveConnection once set.
func (nmac *NMActiveConnection) SpecificObject() (path dbus.ObjectPath, err error) {
	var v dbus.Variant
	if err = nmac.Call("org.freedesktop.DBus.Properties.Get", 0, nmac.iface, "SpecificObject").Store(&v); err == nil {
		var ok bool
		if path, ok = v.Value().(dbus.ObjectPath); !ok {
			err = PropertyTypeError
		}
	}
	return path, err
}

//The ID of the connection, provided as a convenience so that clients
//do not have to retrieve all connection details.
func (nmac *NMActiveConnection) Id() (id string, err error) {
	var v dbus.Variant
	if err = nmac.Call("org.freedesktop.DBus.Properties.Get", 0, nmac.iface, "Id").Store(&v); err == nil {
		var ok bool
		if id, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return id, err
}

//The UUID of the connection, provided as a convenience so that clients
//do not have to retrieve all connection details.
func (nmac *NMActiveConnection) Uuid() (uuid string, err error) {
	var v dbus.Variant
	if err = nmac.Call("org.freedesktop.DBus.Properties.Get", 0, nmac.iface, "Uuid").Store(&v); err == nil {
		var ok bool
		if uuid, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return uuid, err
}

//The type of the connection, provided as a convenience so that clients
//do not have to retrieve all connection details.
func (nmac *NMActiveConnection) Type() (typ string, err error) {
	var v dbus.Variant
	if err = nmac.Call("org.freedesktop.DBus.Properties.Get", 0, nmac.iface, "Type").Store(&v); err == nil {
		var ok bool
		if typ, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return typ, err
}

//Array of object paths representing devices which are part of this active
//connection.
func (nmac *NMActiveConnection) Devices() (list []*NMDevice, err error) {
	var v dbus.Variant
	if err = nmac.Call("org.freedesktop.DBus.Properties.Get", 0, nmac.iface, "Devices").Store(&v); err == nil {
		if paths, ok := v.Value().([]dbus.ObjectPath); ok {
			var device *NMDevice
			for _, path := range paths {
				if device, err = NewNMDevice(nmac.Ctx(), path); err == nil {
					list = append(list, device)
				}
			}
		} else {
			err = PropertyTypeError
		}
	}
	return list, err
}

//The state of this active connection.
func (nmac *NMActiveConnection) State() (state NMActiveConnectionState, err error) {
	var v dbus.Variant
	var s uint32
	if err = nmac.Call("org.freedesktop.DBus.Properties.Get", 0, nmac.iface, "State").Store(&v); err == nil {
		var ok bool
		if s, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			state = NMActiveConnectionState(s)
		}
	}
	return state, err
}

//Whether this active connection is the default IPv4 connection, i.e.
//whether it currently owns the default IPv4 route.
func (nmac *NMActiveConnection) Default() (def bool, err error) {
	var v dbus.Variant
	if err = nmac.Call("org.freedesktop.DBus.Properties.Get", 0, nmac.iface, "Default").Store(&v); err == nil {
		var ok bool
		if def, ok = v.Value().(bool); !ok {
			err = PropertyTypeError
		}
	}
	return def, err
}

//The path to the master device if the connection is a slave.
func (nmac *NMActiveConnection) Master(ctx context.Context) (master *NMDevice, err error) {
	var v dbus.Variant
	if err = nmac.Call("org.freedesktop.DBus.Properties.Get", 0, nmac.iface, "Master").Store(&v); err == nil {
		if path, ok := v.Value().(dbus.ObjectPath); ok {
			master, err = NewNMDevice(ctx, path)
		} else {
			err = PropertyTypeError
		}
	}
	return master, err
}
