package goNetworkManager

import (
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

type NMAccessPoint struct {
	*NMObject
}

//Flags describing the general capabilities of the access point.
type NMAccessPointFlags uint32

const (
	WIFI_AP_FLAGS_NONE    NMAccessPointFlags = 0x0
	WIFI_AP_FLAGS_PRIVACY NMAccessPointFlags = 0x1
)

//Flags describing the security capabilities of the access point.
type NMAccessPointSecFlags uint32

const (
	WIFI_AP_SEC_NONE            NMAccessPointSecFlags = 0x0
	WIFI_AP_SEC_PAIR_WEP40      NMAccessPointSecFlags = 0x1
	WIFI_AP_SEC_PAIR_WEP104     NMAccessPointSecFlags = 0x2
	WIFI_AP_SEC_PAIR_TKIP       NMAccessPointSecFlags = 0x4
	WIFI_AP_SEC_PAIR_CCMP       NMAccessPointSecFlags = 0x8
	WIFI_AP_SEC_GROUP_WEP40     NMAccessPointSecFlags = 0x10
	WIFI_AP_SEC_GROUP_WEP104    NMAccessPointSecFlags = 0x20
	WIFI_AP_SEC_GROUP_TKIP      NMAccessPointSecFlags = 0x40
	WIFI_AP_SEC_GROUP_CCMP      NMAccessPointSecFlags = 0x80
	WIFI_AP_SEC_KEY_MGMT_PSK    NMAccessPointSecFlags = 0x100
	WIFI_AP_SEC_KEY_MGMT_802_1X NMAccessPointSecFlags = 0x200
)

type NMAccessPointWiFiMode uint32

const (
	NM_802_11_MODE_UNKNOWN NMAccessPointWiFiMode = 0
	NM_802_11_MODE_ADHOC   NMAccessPointWiFiMode = 1
	NM_802_11_MODE_INFRA   NMAccessPointWiFiMode = 2
	NM_802_11_MODE_AP      NMAccessPointWiFiMode = 3
)

func finalizeNMAccessPoint(nmap *NMAccessPoint) {
	nmap.Close()
}

func NewNMAccessPoint(ctx context.Context, path dbus.ObjectPath) (nmap *NMAccessPoint, err error) {
	obj, err := NewNMObject(ctx, "org.freedesktop.NetworkManager", path, "org.freedesktop.NetworkManager.AccessPoint")
	if err == nil {
		nmap = new(NMAccessPoint)
		nmap.NMObject = obj
		runtime.SetFinalizer(nmap, finalizeNMAccessPoint)
	}
	return nmap, err
}

//Flags describing the capabilities of the access point.
func (nmap *NMAccessPoint) Flags() (flags NMAccessPointFlags, err error) {
	var v dbus.Variant
	var f uint32
	if err = nmap.Call("org.freedesktop.DBus.Properties.Get", 0, nmap.iface, "Flags").Store(&v); err == nil {
		var ok bool
		if f, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			flags = NMAccessPointFlags(f)
		}
	}
	return flags, err
}

//Flags describing the access point's capabilities according to WPA (Wifi Protected Access).
func (nmap *NMAccessPoint) WpaFlags() (flags NMAccessPointSecFlags, err error) {
	var v dbus.Variant
	var f uint32
	if err = nmap.Call("org.freedesktop.DBus.Properties.Get", 0, nmap.iface, "WpaFlags").Store(&v); err == nil {
		var ok bool
		if f, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			flags = NMAccessPointSecFlags(f)
		}
	}
	return flags, err
}

//Flags describing the access point's capabilities according to the RSN (Robust Secure Network) protocol.
func (nmap *NMAccessPoint) RsnFlags() (flags NMAccessPointSecFlags, err error) {
	var v dbus.Variant
	var f uint32
	if err = nmap.Call("org.freedesktop.DBus.Properties.Get", 0, nmap.iface, "RsnFlags").Store(&v); err == nil {
		var ok bool
		if f, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			flags = NMAccessPointSecFlags(f)
		}
	}
	return flags, err
}

//The Service Set Identifier identifying the access point.
func (nmap *NMAccessPoint) Ssid() (ssid string, err error) {
	var v dbus.Variant
	if err = nmap.Call("org.freedesktop.DBus.Properties.Get", 0, nmap.iface, "Ssid").Store(&v); err == nil {
		if s, ok := v.Value().([]byte); !ok {
			err = PropertyTypeError
		} else {
			ssid = string(s)
		}
	}
	return ssid, err
}

//The radio channel frequency in use by the access point, in MHz.
func (nmap *NMAccessPoint) Frequency() (freq uint32, err error) {
	var v dbus.Variant
	if err = nmap.Call("org.freedesktop.DBus.Properties.Get", 0, nmap.iface, "Frequency").Store(&v); err == nil {
		var ok bool
		if freq, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		}
	}
	return freq, err
}

//The hardware address (BSSID) of the access point.
func (nmap *NMAccessPoint) HwAddress() (hwaddr string, err error) {
	var v dbus.Variant
	if err = nmap.Call("org.freedesktop.DBus.Properties.Get", 0, nmap.iface, "HwAddress").Store(&v); err == nil {
		var ok bool
		if hwaddr, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return hwaddr, err
}

//Describes the operating mode of the access point.
func (nmap *NMAccessPoint) Mode() (mode NMAccessPointWiFiMode, err error) {
	var v dbus.Variant
	var m uint32
	if err = nmap.Call("org.freedesktop.DBus.Properties.Get", 0, nmap.iface, "Mode").Store(&v); err == nil {
		var ok bool
		if m, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			mode = NMAccessPointWiFiMode(m)
		}
	}
	return mode, err
}

//The maximum bitrate this access point is capable of, in kilobits/second (Kb/s).
func (nmap *NMAccessPoint) MaxBitrate() (bitrate uint32, err error) {
	var v dbus.Variant
	if err = nmap.Call("org.freedesktop.DBus.Properties.Get", 0, nmap.iface, "MaxBitrate").Store(&v); err == nil {
		var ok bool
		if bitrate, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		}
	}
	return bitrate, err
}

//The current signal quality of the access point, in percent.
func (nmap *NMAccessPoint) Strength() (strength byte, err error) {
	var v dbus.Variant
	if err = nmap.Call("org.freedesktop.DBus.Properties.Get", 0, nmap.iface, "Strength").Store(&v); err == nil {
		var ok bool
		if strength, ok = v.Value().(byte); !ok {
			err = PropertyTypeError
		}
	}
	return strength, err
}
