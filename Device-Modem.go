package goNetworkManager

import (
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

type NMDeviceModem struct {
	*NMDevice
}

//Flags describing one or more of the general access technology families
//that a modem device supports.
type NMDeviceModemCapabilites uint32

const (
	DEVICE_MODEM_CAPABILITIES_NONE      NMDeviceModemCapabilites = 0x0
	DEVICE_MODEM_CAPABILITIES_POTS      NMDeviceModemCapabilites = 0x1
	DEVICE_MODEM_CAPABILITIES_CDMA_EVDO NMDeviceModemCapabilites = 0x2
	DEVICE_MODEM_CAPABILITIES_GSM_UMTS  NMDeviceModemCapabilites = 0x4
	DEVICE_MODEM_CAPABILITIES_LTE       NMDeviceModemCapabilites = 0x8
)

func finalizeNMDeviceModem(nmdm *NMDeviceModem) {
	nmdm.Close()
}

func NewNMDeviceModem(ctx context.Context, path dbus.ObjectPath) (nmdm *NMDeviceModem, err error) {
	obj, err := NewNMObject(ctx, "org.freedesktop.NetworkManager", path, "org.freedesktop.NetworkManager.Device.Modem")
	if err == nil {
		nmd := new(NMDevice)
		nmd.NMObject = obj
		nmdm = new(NMDeviceModem)
		nmdm.NMDevice = nmd
		runtime.SetFinalizer(nmdm, finalizeNMDeviceModem)
	}
	return nmdm, err
}

//The generic family of access technologies the modem supports.  Not all
//capabilities are available at the same time however; some modems require
//a firmware reload or other reinitialization to switch between eg CDMA/EVDO
//and GSM/UMTS.
func (nmdm *NMDeviceModem) ModemCapabilities() (modemcapabilities NMDeviceModemCapabilites, err error) {
	var v dbus.Variant
	var c uint32
	if err = nmdm.Call("org.freedesktop.DBus.Properties.Get", 0, nmdm.iface, "ModemCapabilities").Store(&v); err == nil {
		var ok bool
		if c, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		} else {
			modemcapabilities = NMDeviceModemCapabilites(c)
		}
	}
	return modemcapabilities, err
}

//The generic family of access technologies the modem currently supports
//without a firmware reload or reinitialization.
func (nmdm *NMDeviceModem) CurrentCapabilities() (currentcapabilities NMDeviceModemCapabilites, err error) {
	var v dbus.Variant
	if err = nmdm.Call("org.freedesktop.DBus.Properties.Get", 0, nmdm.iface, "CurrentCapabilities").Store(&v); err == nil {
		var ok bool
		if currentcapabilities, ok = v.Value().(NMDeviceModemCapabilites); !ok {
			err = PropertyTypeError
		}
	}
	return currentcapabilities, err
}
