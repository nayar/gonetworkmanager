package goNetworkManager

import (
	"reflect"
	"runtime"

	"github.com/godbus/dbus"
	"golang.org/x/net/context"
)

type NetworkManager struct {
	*NMObject
}

func finalizeNetworkManager(nm *NetworkManager) {
	nm.Close()
}

func NewNetworkManager(ctx context.Context) (nm *NetworkManager, err error) {
	obj, err := NewNMObject(ctx, "org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager", "org.freedesktop.NetworkManager")
	if err == nil {
		nm = new(NetworkManager)
		nm.NMObject = obj
		runtime.SetFinalizer(nm, finalizeNetworkManager)
	}
	return nm, err
}

func (nm *NetworkManager) CheckPermissions(ctx context.Context) <-chan struct{} {
	c := make(chan struct{}, 1)
	go nm.signalTaskCtx(ctx, "CheckPermissions", nm.checkPermissionsHandler, c)
	return c
}

func (nm *NetworkManager) StateChanged(ctx context.Context) <-chan uint32 {
	c := make(chan uint32, 1)
	go nm.signalTaskCtx(ctx, "StateChanged", nm.stateChangedHandler, c)
	return c
}

func (nm *NetworkManager) DeviceAdded(ctx context.Context) <-chan *NMDevice {
	c := make(chan *NMDevice, 1)
	go nm.signalTaskCtx(ctx, "DeviceAdded", nm.deviceAddedHandler, c)
	return c
}

func (nm *NetworkManager) DeviceRemoved(ctx context.Context) <-chan *NMDevice {
	c := make(chan *NMDevice, 1)
	go nm.signalTaskCtx(ctx, "DeviceRemoved", nm.deviceRemovedHandler, c)
	return c
}

func (nm *NetworkManager) checkPermissionsHandler(ctx context.Context, v []interface{}, c interface{}) {
	if checkPermissionsChan, ok := c.(chan struct{}); ok {
		select {
		case <-ctx.Done():
		case checkPermissionsChan <- struct{}{}:
		default:
		}
	}
}

func (nm *NetworkManager) stateChangedHandler(ctx context.Context, v []interface{}, c interface{}) {
	if len(v) == 1 {
		if stateChangedChan, ok := c.(chan uint32); ok {
			if state, ok := v[0].(uint32); ok {
				select {
				case <-ctx.Done():
				case stateChangedChan <- state:
				default:
					<-stateChangedChan
					stateChangedChan <- state
				}
			}
		}
	}
}

func (nm *NetworkManager) deviceAddedHandler(ctx context.Context, v []interface{}, c interface{}) {
	if len(v) == 1 {
		if deviceAddedChan, ok := c.(chan *NMDevice); ok {
			if path, ok := v[0].(dbus.ObjectPath); ok {
				if device, err := NewNMDevice(ctx, path); err == nil {
					select {
					case <-ctx.Done():
					case deviceAddedChan <- device:
					default:
						d := <-deviceAddedChan
						d.Close()
						deviceAddedChan <- device
					}
				}
			}
		}
	}
}

func (nm *NetworkManager) deviceRemovedHandler(ctx context.Context, v []interface{}, c interface{}) {
	if len(v) == 1 {
		if deviceRemovedChan, ok := c.(chan *NMDevice); ok {
			if path, ok := v[0].(dbus.ObjectPath); ok {
				if device, err := NewNMDevice(ctx, path); err == nil {
					select {
					case <-ctx.Done():
					case deviceRemovedChan <- device:
					default:
						<-deviceRemovedChan
						deviceRemovedChan <- device
					}
				}
			}
		}
	}
}

func (nm *NetworkManager) GetDevices(ctx context.Context) (list []*NMDevice, err error) {
	var paths []dbus.ObjectPath
	if err = nm.Call(nm.iface+".GetDevices", 0).Store(&paths); err == nil {
		var device *NMDevice
		for _, path := range paths {
			if device, err = NewNMDevice(ctx, path); err == nil {
				list = append(list, device)
			} else {
				break
			}
		}
	}
	return list, err
}

func (nm *NetworkManager) GetDeviceByIpIface(ctx context.Context, iface string) (device *NMDevice, err error) {
	var path dbus.ObjectPath
	if err = nm.Call(nm.iface+".GetDeviceByIpIface", 0, iface).Store(&path); err == nil {
		device, err = NewNMDevice(ctx, path)
	}
	return device, err
}

func (nm *NetworkManager) ActivateConnection(ctx context.Context, connection *NMSettingsConnection, device *NMDevice) (active_connection *NMActiveConnection, err error) {
	var path dbus.ObjectPath
	if err = nm.Call(nm.iface+".ActivateConnection", 0, dbus.ObjectPath(connection.Path()), dbus.ObjectPath(device.Path()), dbus.ObjectPath("/")).Store(&path); err == nil {
		active_connection, err = NewNMActiveConnection(ctx, path)
	}
	return active_connection, err
}

func (nm *NetworkManager) AddAndActivateConnection(ctx context.Context, connection map[string]map[string]interface{}, device *NMDevice) (conn *NMSettingsConnection, active_connection *NMActiveConnection, err error) {
	var connPath, ActiveConnPath dbus.ObjectPath
	connectionVariant := make(map[string]map[string]dbus.Variant)
	for k1, _ := range connection {
		connectionVariant[k1] = make(map[string]dbus.Variant)
		for k2, v := range connection[k1] {
			connectionVariant[k1][k2] = dbus.MakeVariant(v)
		}
	}
	if err = nm.Call(nm.iface+".AddAndActivateConnection", 0, connectionVariant, dbus.ObjectPath(device.Path()), "/").Store(&connPath, &ActiveConnPath); err == nil {
		if conn, err = NewNMSettingsConnection(ctx, connPath); err == nil {
			active_connection, err = NewNMActiveConnection(ctx, ActiveConnPath)
			conn.Close()
		}
	}
	return conn, active_connection, err
}

func (nm *NetworkManager) DeactivateConnection(connection *NMSettingsConnection) (err error) {
	return nm.Call(nm.iface+".DeactivateConnection", 0, dbus.ObjectPath(connection.Path())).Err
}

func (nm *NetworkManager) Sleep(sleep bool) (err error) {
	return nm.Call(nm.iface+".Sleep", 0, sleep).Err
}

func (nm *NetworkManager) Enable(enable bool) (err error) {
	return nm.Call(nm.iface+".Enable", 0, enable).Err
}

func (nm *NetworkManager) GetPermissions() (permissions map[string]string, err error) {
	err = nm.Call(nm.iface+".GetPermissions", 0).Store(&permissions)
	return permissions, err
}

func (nm *NetworkManager) SetLogging(domains string) (err error) {
	return nm.Call(nm.iface+".SetLogging", 0, domains).Err
}

func (nm *NetworkManager) GetLogging() (level string, domains string, err error) {
	err = nm.Call(nm.iface+".GetLogging", 0).Store(&level, &domains)
	return level, domains, err
}

func (nm *NetworkManager) CheckConnectivity() (connectivity uint32, err error) {
	err = nm.Call(nm.iface+".CheckConnectivity", 0).Store(&connectivity)
	return connectivity, err
}

func (nm *NetworkManager) Devices(ctx context.Context) (list []*NMDevice, err error) {
	var v dbus.Variant
	if err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "Devices").Store(&v); err == nil {
		if paths, ok := v.Value().([]dbus.ObjectPath); ok {
			var device *NMDevice
			for _, path := range paths {
				if device, err = NewNMDevice(ctx, path); err == nil {
					list = append(list, device)
				} else {
					break
				}
			}
		} else {
			err = PropertyTypeError
		}
	}
	return list, err
}

func (nm *NetworkManager) NetworkingEnabled() (enabled bool, err error) {
	var v dbus.Variant
	err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "NetworkingEnabled").Store(&v)
	return reflect.DeepEqual(v.Value(), true), err
}

func (nm *NetworkManager) WirelessEnabled() (enabled bool, err error) {
	var v dbus.Variant
	err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "WirelessEnabled").Store(&v)
	return reflect.DeepEqual(v.Value(), true), err
}

func (nm *NetworkManager) SetWirelessEnabled(enabled bool) (err error) {
	v := dbus.MakeVariant(enabled)
	return nm.Call("org.freedesktop.DBus.Properties.Set", 0, nm.iface, "WirelessEnabled", v).Err
}

func (nm *NetworkManager) WirelessHardwareEnabled() (enabled bool, err error) {
	var v dbus.Variant
	err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "WirelessHardwareEnabled").Store(&v)
	return reflect.DeepEqual(v.Value(), true), err
}

func (nm *NetworkManager) WwanEnabled() (enabled bool, err error) {
	var v dbus.Variant
	err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "WwanEnabled").Store(&v)
	return reflect.DeepEqual(v.Value(), true), err
}

func (nm *NetworkManager) SetWwanEnabled(enabled bool) (err error) {
	v := dbus.MakeVariant(enabled)
	return nm.Call("org.freedesktop.DBus.Properties.Set", 0, nm.iface, "WwanEnabled", v).Err
}

func (nm *NetworkManager) WwanHardwareEnabled() (enabled bool, err error) {
	var v dbus.Variant
	err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "WwanHardwareEnabled").Store(&v)
	return reflect.DeepEqual(v.Value(), true), err
}

func (nm *NetworkManager) WimaxEnabled() (enabled bool, err error) {
	var v dbus.Variant
	err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "WimaxEnabled").Store(&v)
	return reflect.DeepEqual(v.Value(), true), err
}

func (nm *NetworkManager) SetWimaxEnabled(enabled bool) (err error) {
	v := dbus.MakeVariant(enabled)
	return nm.Call("org.freedesktop.DBus.Properties.Set", 0, nm.iface, "WimaxEnabled", v).Err
}

func (nm *NetworkManager) WimaxHardwareEnabled() (enabled bool, err error) {
	var v dbus.Variant
	err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "WimaxHardwareEnabled").Store(&v)
	return reflect.DeepEqual(v.Value(), true), err
}

func (nm *NetworkManager) ActiveConnections(ctx context.Context) (list []*NMActiveConnection, err error) {
	var v dbus.Variant
	if err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "ActiveConnections").Store(&v); err == nil {
		if paths, ok := v.Value().([]dbus.ObjectPath); ok {
			var ac *NMActiveConnection
			for _, path := range paths {
				if ac, err = NewNMActiveConnection(ctx, path); err == nil {
					list = append(list, ac)
				} else {
					break
				}
			}
		} else {
			err = PropertyTypeError
		}
	}
	return list, err
}

func (nm *NetworkManager) PrimaryConnection(ctx context.Context) (ac *NMActiveConnection, err error) {
	var v dbus.Variant
	if err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "PrimaryConnection").Store(&v); err == nil {
		if path, ok := v.Value().(dbus.ObjectPath); ok {
			ac, err = NewNMActiveConnection(ctx, path)
		} else {
			err = PropertyTypeError
		}
	}
	return ac, err
}

func (nm *NetworkManager) ActivatingConnection(ctx context.Context) (ac *NMActiveConnection, err error) {
	var v dbus.Variant
	if err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "ActivatingConnection").Store(&v); err == nil {
		if path, ok := v.Value().(dbus.ObjectPath); ok {
			ac, err = NewNMActiveConnection(ctx, path)
		} else {
			err = PropertyTypeError
		}
	}
	return ac, err
}

func (nm *NetworkManager) Startup() (startup bool, err error) {
	var v dbus.Variant
	if err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "Startup").Store(&v); err == nil {
		var ok bool
		if startup, ok = v.Value().(bool); !ok {
			err = PropertyTypeError
		}
	}
	return startup, err
}

func (nm *NetworkManager) Version() (version string, err error) {
	var v dbus.Variant
	if err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "Version").Store(&v); err == nil {
		var ok bool
		if version, ok = v.Value().(string); !ok {
			err = PropertyTypeError
		}
	}
	return version, err
}

func (nm *NetworkManager) State() (state uint32, err error) {
	var v dbus.Variant
	if err = nm.Call("org.freedesktop.DBus.Properties.Get", 0, nm.iface, "State").Store(&v); err == nil {
		var ok bool
		if state, ok = v.Value().(uint32); !ok {
			err = PropertyTypeError
		}
	}
	return state, err
}

const (
	NM_STATE_UNKNOWN          uint32 = 0
	NM_STATE_ASLEEP           uint32 = 10
	NM_STATE_DISCONNECTED     uint32 = 20
	NM_STATE_DISCONNECTING    uint32 = 30
	NM_STATE_CONNECTING       uint32 = 40
	NM_STATE_CONNECTED_LOCAL  uint32 = 50
	NM_STATE_CONNECTED_SITE   uint32 = 60
	NM_STATE_CONNECTED_GLOBAL uint32 = 70
)
